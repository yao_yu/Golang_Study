#Golang学习
##基础
###包、变量和函数
#### 包
	`package` XXX
	导入 `import` ("xxx")
	导出名 首字母大写的名称是被导出的
#### 函数
	`func` xxx(x, y int)(c, d int){}
	当两个或多个连续的函数命名参数是同一类型，则除了最后一个类型之外，其他都可以省略。
	函数可以返回任意数量的返回值
	返回值可以被命名，并且像变量那样使用
#### 变量
	`var` c, python, java bool
	`var` i, j int = 1, 2
	`var` c, python, java = true, false, "no!"
	- 短声明变量
		var i, j int = 1, 2
		k := 3
		c, python, java := true, false, "no!"
	函数外的每个语句都必须以关键字开始（`var`、`func`、等等），`:=` 结构不能使用在函数外。
	- 基本类型
	- 零值
		数值类型为 `0`，
		布尔类型为 `false`，
		字符串为 `""`（空字符串）。
	- 类型转换
		`T`(v)
	- 类型推导
	- 常量
		`const`

###流程控制语句：for、if、else 和 switch
####for
	for i := 0; i < 10; i++ {}
	for i < 10 {}   (<==> while)
####if
	if x < 0 {}
	if v := math.Pow(x, n); v < lim {}
	if else
####switch
	fallthrough
	没有条件的switch <==> if-then-else
####defer
	延迟执行,按栈方式后进先出执行
	
###复杂类型: struct, slice和map
####
	
