#!/usr/bin/env python
# -*- coding:utf-8 -*-

def fibonacci(num):
    if num < 2:
        return 1
    return fibonacci(num - 1) + fibonacci(num - 2)

if __name__ == '__main__':
    import time
    class CElapseTime(object):
        def __init__(self, prompt):
            self.start = time.time()
            self.prompt = prompt
        def __del__(self):
            print('%s: %s' % (self.prompt, time.time() - self.start))

    def main():
        aTimer = CElapseTime('fib')
        nums = 0
        for i in range(8):
            nums = fibonacci(30)
            print(nums)
    
    main()