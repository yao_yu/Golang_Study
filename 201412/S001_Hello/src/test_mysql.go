package main

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"time"
)

func CheckErr(err error) {
	if err != nil {
		fmt.Println(err.Error())
		panic(err)
	}
}

func main() {
	db, err := sql.Open("mysql", "root:`123456@tcp(192.168.6.222:3306)/ssglxmgl?charset=utf8")
	CheckErr(err)
	defer db.Close()

	t := time.Now()
	rows, err := db.Query("select * from t_htgl_htgcl")
	CheckErr(err)
	cols, _ := rows.Columns()
	for i := range cols {
		fmt.Printf("%v \t", cols[i])
	}

	values := make([]sql.RawBytes, len(cols))
	scans := make([]interface{}, len(cols))
	for i := range values {
		scans[i] = &values[i]
	}
	results := make(map[int]map[string]string)

	n := 0
	i := 0
	for rows.Next() {
		if err := rows.Scan(scans...); err != nil {
			fmt.Println("错误")
		}
		row := make(map[string]string)
		for j, v := range values {
			key := cols[j]
			row[key] = string(v)
		}
		results[i] = row
		i++
		n++
	}

	for _, j := range results {
		fmt.Println(j)
	}

	fmt.Println()
	fmt.Println(n, time.Now().Sub(t))

}
