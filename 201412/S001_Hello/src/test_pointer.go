package main

import "fmt"

func main() {
	test_pointers()
}

func zeroval(ival int) {
	ival = 0
}

func zeroptr(iptr *int) {
	*iptr = 0
}

func test_pointers() {
	i := 1
	fmt.Println("初始值:", i)

	zeroval(i)
	fmt.Println("zeroval:", i)

	zeroptr(&i)
	fmt.Println("zeroptr:", i)

	fmt.Println("指针:", &i)
}
