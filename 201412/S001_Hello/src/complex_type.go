package main

import (
	"fmt"
	"runtime"
	"time"
)

func fibonacci(num int) int {
	if num < 2 {
		return 1
	}
	return fibonacci(num-1) + fibonacci(num-2) //我不记得golang有没有三目运算符了......
}
func main() {
	ch := make(chan int, 8)
	runtime.GOMAXPROCS(8)
	start := time.Now()
	for i := 0; i < 8; i++ {
		go func() {
			nums := fibonacci(40)
			ch <- nums
		}()
		//fmt.Println(nums)
	}
	for i := 0; i < 8; i++ {
		fmt.Println(<-ch)
	}
	end := time.Now()
	fmt.Println("total time:", end.Sub(start)) //.Nanoseconds()/1000/1000)
}
