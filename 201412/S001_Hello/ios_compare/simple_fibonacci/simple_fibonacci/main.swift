//
//  main.swift
//  simple_fibonacci
//
//  Created by yao_yu on 14/12/14.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import Foundation

func fibonacci(n: Int64) -> Int64{
    if n < 2 {
        return 1
    }
    return fibonacci(n-1) + fibonacci(n-2)
}

func main(){
    var start = NSDate()
    var nums:Int64 = 0
    var queue:dispatch_queue_t
    for i in 0..<10{
        queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
        dispatch_async(queue, { () -> Void in
            nums = fibonacci(40)
            println(nums)
            NSLog("下载图片1----%@,%@",NSThread.currentThread(), nums);
        })
    }
    NSLog("下载图片1----%@,%@",NSThread.mainThread(), nums);
    
    var b = NSDate().timeIntervalSinceDate(start)
    println(b)
}

main()